var request = require('request');
var cheerio = require('cheerio');
var fs=require('fs');
var csv = require('ya-csv');
var url = 'https://www.medium.com/';
var util=require('util');
var writer = csv.createCsvStreamWriter(fs.createWriteStream('output-sync.csv'));

var maxClients = 5;
var currentClients = 0;
var _pending = [];

request(url, function(err, resp, body){
  var count=0;
  $ = cheerio.load(body);
  links = $('a'); 
  count=0;
  var data=[];
  var mp={};
  $(links).each(function(i, link){
    // console.log($(link).text() + ':\n  ' + $(link).attr('href'));
  var l= $(link).attr('href');
  if(l.includes('http')){
  if(mp[l]!="1"){
      data.push(l);
   
    writer.writeRecord([l]);
    mp[l]="1";
  } }
 })
 var urls = data;
 
console.log(urls.length);
//console.log(urls);
urls.forEach(function(url){
   makeRequest(url);
    
})

  
 
 
 
function process_pending()
{ 
   if (_pending.length > 0) {
    var cb = _pending.shift();
    currentClients++;
    cb(function() {
      currentClients--;
      process_pending();
    });
  }
}

function client_limit(cb, req, res) 
{
  if (currentClients <= maxClients) {
    currentClients++;
    cb(function() {
      currentClients--;
      process_pending();
     // console.log("")
    }, req, res);
  }
  else {
    console.log("Number of clients are..."+currentClients);
    console.log('Overloaded, queuing clients...');
    _pending.push(cb);
  }
} 
 




function makeRequest(url){
    var lnk=url;
    if(lnk.includes('medium')){
  request(lnk,function(err,resp,body){
        if(err) throw err;
        $=cheerio.load(body);
        links = $('a'); 
       //var mp={};
       client_limit(function(done){
        $(links).each(function(i, link){
        // console.log($(link).text() + ':\n  ' + $(link).attr('href'));
        var l= $(link).attr('href');
        if(l.includes('http')){
         count++;
         if(mp[l]!="1"){
         writer.writeRecord([l]);
         mp[l]="1";
         } }
       
    })
        done(); 
       })
    //resp.end();
    
    });
    } 
   
}

})


